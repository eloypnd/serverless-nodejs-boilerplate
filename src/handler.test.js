import { hello } from './handler';

test('inital test', async () => {
  expect.assertions(1);
  expect(await hello()).toStrictEqual(
    expect.objectContaining({ statusCode: 200 })
  );
});

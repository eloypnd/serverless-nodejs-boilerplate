# Serverless [Node.js](https://nodejs.org/en/) boilerplate

- Infrastructure as a code with [Serverless Framework](https://serverless.com/)
- Linting based on [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) and [Prettier](https://prettier.io/).
- Modern JavaScript thanks to [webpack](https://webpack.js.org/) and [Babel](https://babeljs.io/).
- Unit testing with [Jest](https://jestjs.io/)
